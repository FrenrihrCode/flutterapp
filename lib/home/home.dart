import 'package:flutter/material.dart';
import 'package:trips_app/home/screens/histories.dart';
import 'package:trips_app/home/screens/post_list.dart';
import 'package:trips_app/widgets/clipper_header.dart';
import 'package:trips_app/widgets/gradient_background.dart';

class Home extends StatelessWidget {
  final String descripcionPrueba =
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum temporibus fugit voluptas at perferendis nemo, eiu incidunt distinctio repellat laboriosam reiciendis vel porro impedit officiis adipisci, tempore vero hic obcaecati! Alias quas quam quaerat pariatur excepturi officia quibusdam, quia impedit placeat qui nostrum iure quod illum sunt possimus, molestias exercitationem.";

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        /* Column(
          children: <Widget>[
            headerList,
            Expanded(
              child: PostList(),
            )
          ],
        ), */
        PostList(),
        Histories(),
        ClipPath(
          clipper: HeaderClipper(),
          child: GradientBackground('Novedades'),
        ),
      ],
    );
  }
}
