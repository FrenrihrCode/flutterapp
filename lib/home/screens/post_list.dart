import 'package:flutter/material.dart';
import 'package:trips_app/widgets/single_post.dart';

class PostList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double heightTop = MediaQuery.of(context).size.height * 0.2;
    return ListView(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: heightTop)),
        SinglePost('https://stat.ameba.jp/user_images/20191026/10/aoilena/65/9a/p/o1680105014624005984.png?caw=800'),
        SinglePost('https://i.imgur.com/wrjZcyB.png'),
        SinglePost('https://ecchihunter.com/wp-content/uploads/2019/10/Azur-Lane-Ep4-Wallpaper-7.jpg'),
        SinglePost('https://i.redd.it/ovc1sm3p82j41.png'),
        SinglePost('https://besthqwallpapers.com/Uploads/16-5-2020/132758/thumb2-queen-elizabeth-hood-warspite-spring-azur-lane.jpg')
      ],
    );
  }
}
