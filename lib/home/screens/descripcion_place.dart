import 'package:flutter/material.dart';
import 'package:trips_app/home/screens/rating.dart';
import 'package:trips_app/widgets/solid_button.dart';

class DesciptionPlace extends StatelessWidget {
  final String description;
  final String namePlace;
  final int starQauntity;

  DesciptionPlace(this.namePlace, this.starQauntity, this.description);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height * 0.15;

    final titleStars = Container(
      margin: EdgeInsets.only(top: height),
      child: Column(
        children: <Widget>[
          Text(
            namePlace,
            style: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.w900,
                fontFamily: 'Pacifico'),
            textAlign: TextAlign.center,
          ),
          Rating(rating: 4.0,)
        ],
      ),
    );

    final descriptionContainer = Container(
      margin: EdgeInsets.only(top: 15, left: 20, right: 20),
      child: Text(
        description,
        style: TextStyle(fontSize: 16, color: Color(0xFF56575a)),
      ),
    );

    final desciptionPlace = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        titleStars,
        descriptionContainer,
        SolidButton('Navegar', (){print('Navegando');})
      ],
    );

    return desciptionPlace;
  }
}
