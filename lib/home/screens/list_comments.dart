import 'package:flutter/material.dart';
import 'package:trips_app/home/screens/comment.dart';

class ListComments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Comment(
            'Nagato 1',
            '2 reviews - 3 photos',
            'Este lugar es muy asombroso',
            'https://static.zerochan.net/Nagato.%28Azur.Lane%29.full.2752710.png'),
        Comment(
            'Nagato 2',
            '5 reviews - 1 photos',
            'Maginifico, me qeudo maravillada',
            'https://i.imgur.com/f0XVU0t.jpg'),
        Comment(
            'Nagato 2',
            '1 reviews - 5 photos',
            'x2 :V',
            'https://static.zerochan.net/Nagato.%28Azur.Lane%29.full.2555341.jpg')
      ],
    );
  }
}
