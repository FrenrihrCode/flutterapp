import 'package:flutter/material.dart';

class Comment extends StatelessWidget {
  final String avatarUrl;
  final String userName;
  final String userDetail;
  final String userComment;

  Comment(this.userName, this.userDetail, this.userComment, this.avatarUrl);

  @override
  Widget build(BuildContext context) {
    final userCommentContainer = Container(
      margin: EdgeInsets.fromLTRB(20.0, 10.0, 0, 0),
      child: Text(
        userComment,
        textAlign: TextAlign.left,
        style: TextStyle(fontSize: 14.0),
      ),
    );

    final userDetailContainer = Container(
      margin: EdgeInsets.fromLTRB(20.0, 0, 0, 0),
      child: Text(
        userDetail,
        textAlign: TextAlign.left,
        style: TextStyle(fontSize: 14.0, color: Color(0xFFa3a5a7)),
      ),
    );

    final userNameContainer = Container(
      margin: EdgeInsets.fromLTRB(20.0, 0, 0, 0),
      child: Text(
        userName,
        textAlign: TextAlign.left,
        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w900),
      ),
    );

    final photoContainer = Container(
      margin: EdgeInsets.only(top: 20.0, left: 20.0),
      width: 80.0,
      height: 80.0,
      child: CircleAvatar(
        backgroundImage: NetworkImage(avatarUrl),
      ),
    );

    final userContainer = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        userNameContainer,
        userDetailContainer,
        userCommentContainer
      ],
    );

    return Row(
      children: <Widget>[
        photoContainer,
        userContainer
      ],
    );
  }
}
