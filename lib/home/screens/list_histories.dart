import 'package:flutter/material.dart';

class ListHistories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final avatarHistorie = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircleAvatar(
          radius: 35,
          backgroundColor: Color(0xff008080),
          child: CircleAvatar(
            radius: 30,
            backgroundImage: NetworkImage(
                'https://cdn.myanimelist.net/r/360x360/images/characters/5/391270.jpg?s=6ad396390fe312387da5c7e2826858f2'),
          ),
        ),
        Text('P. Eugen')
      ],
    );
    return Container(
      height: 120.0,
      color: Colors.white,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          avatarHistorie,
          avatarHistorie,
          avatarHistorie,
          avatarHistorie,
          avatarHistorie,
          avatarHistorie,
          avatarHistorie
        ],
      ),
    );
  }
}
