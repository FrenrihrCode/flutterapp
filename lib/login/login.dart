import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trips_app/login/screens/baizer_container.dart';
import 'package:trips_app/login/screens/divider_line.dart';
import 'package:trips_app/login/state/login_state.dart';
import 'package:trips_app/widgets/btn_back.dart';
import 'package:trips_app/widgets/input_field.dart';
import 'package:trips_app/widgets/solid_button.dart';

class LoginPage extends StatefulWidget {
  final String title;
  LoginPage({Key key, this.title}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        InputTextField(
            inputType: TextInputType.emailAddress,
            textEditingController: _emailController,
            title: "Email:"),
        InputTextField(
            inputType: TextInputType.text,
            textEditingController: _passwordController,
            title: "Contraseña:",
            isPassword: true),
      ],
    );
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'R',
          style: TextStyle(
              fontFamily: 'Pacifico',
              fontSize: 32,
              fontWeight: FontWeight.w700,
              color: Color(0xFF07b8ff)),
          children: [
            TextSpan(
              text: 'en',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            TextSpan(
              text: 'tY',
              style: TextStyle(color: Color(0xFF07b8ff), fontSize: 30),
            ),
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Container(
      height: height,
      child: Stack(
        children: <Widget>[
          Positioned(
              top: -height * .15,
              right: -MediaQuery.of(context).size.width * .4,
              child: BaizerContainer()),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: height * .2),
                  _title(),
                  SizedBox(height: 35),
                  _emailPasswordWidget(),
                  SizedBox(height: 20),
                  SolidButton(
                      'INGRESAR',
                      () => Provider.of<LoginState>(context, listen: false)
                          .loginUserEmailPassword(
                              email: _emailController.text,
                              password: _passwordController.text)),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.centerRight,
                    child: Text('Forgot Password ?',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500)),
                  ),
                  DividerLine(
                    phrase: 'o',
                  ),
                  SolidButton(
                      'INGRESAR CON GOOGLE',
                      () => Provider.of<LoginState>(context, listen: false)
                          .loginWithGoogle()),
                ],
              ),
            ),
          ),
          Positioned(top: 40, left: 0, child: BackButtonCustom()),
        ],
      ),
    ));
  }
}
