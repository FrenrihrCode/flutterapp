import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trips_app/login/login.dart';
import 'package:trips_app/login/state/login_state.dart';
import 'package:trips_app/utils/az_trips.dart';

class Landing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LoginState auth = Provider.of<LoginState>(context);
    return StreamBuilder<FirebaseUser>(
      stream: auth.onAuthStateChanged,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          FirebaseUser user = snapshot.data;
          if (user == null) {
            return LoginPage();
          }
          return AzurLaneTrips();
        } else {
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}