import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:trips_app/home/home.dart';
import 'package:trips_app/list/characters.dart';
import 'package:trips_app/profile/profile.dart';

class AzurLaneTrips extends StatefulWidget {
  AzurLaneTrips({Key key}) : super(key: key);

  @override
  _AzurLaneTripsState createState() => _AzurLaneTripsState();
}

class _AzurLaneTripsState extends State<AzurLaneTrips> {
  int _indexTap = 0;
  final List<Widget> widgetsScreen = [
    Home(),
    CharactersView(),
    Profile()
  ];

  void _onTapTapped(int index) {
    setState(() {
      this._indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
              canvasColor: Color(0xE60096d2), primaryColor: Colors.black),
          child: BottomNavigationBar(
              currentIndex: _indexTap,
              onTap: _onTapTapped,
              unselectedItemColor: Colors.white,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                    icon: Icon(Icons.home), title: Text('Inicio')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.list), title: Text('Personajes')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.person), title: Text('Perfil'))
              ],
              
              )
              ),
              
      body: widgetsScreen[_indexTap],
    );
  }
}
