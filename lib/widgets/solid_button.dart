import 'package:flutter/material.dart';

class SolidButton extends StatelessWidget {
  final String btnText;
  final VoidCallback btnAction;

  SolidButton(this.btnText, this.btnAction);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Color(0xFF07b8ff),
      child: InkWell(
          onTap: btnAction,
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: Text(
              btnText,
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          )),
    );
  }
}
