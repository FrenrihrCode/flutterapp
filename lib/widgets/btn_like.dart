import 'package:flutter/material.dart';

class FavoriteButton extends StatefulWidget {
  @override
  _FavoriteButton createState() => _FavoriteButton();
}

class _FavoriteButton extends State<FavoriteButton> {
  bool _favPressed = false;

  void _onPressedFav() {
    setState(() {
      this._favPressed = !this._favPressed;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: Color(0xFF11DA53),
      mini: true,
      tooltip: "Añadir a fovaritos",
      child: Icon(this._favPressed ? Icons.favorite : Icons.favorite_border),
      onPressed: _onPressedFav,
    );
  }
}
