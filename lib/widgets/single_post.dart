import 'package:flutter/material.dart';
import 'package:trips_app/widgets/btn_like.dart';

class SinglePost extends StatelessWidget {
  final String urlPost;
  SinglePost(this.urlPost);
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final post = Container(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: FadeInImage(
            width: width*0.9,
            height: width*0.5,
            image: NetworkImage(urlPost),
            placeholder: AssetImage('assets/loading.gif'),
            fit: BoxFit.cover,
          ))),
      );
    return Stack(
      alignment: Alignment(0.9, 1),
      children: <Widget>[post, FavoriteButton()],
    );
  }
}
