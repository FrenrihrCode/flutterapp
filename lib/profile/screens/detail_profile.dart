import 'package:flutter/material.dart';

class DetailProfile extends StatelessWidget {
  final String avatarUrl;
  final String userEmail;
  final String userName;
  final String userDesc;
  final String userFaction;

  DetailProfile(this.avatarUrl, this.userEmail, this.userName, this.userDesc,
      this.userFaction);

  @override
  Widget build(BuildContext context) {
    final double heightTop = MediaQuery.of(context).size.height * 0.15;

    final avatarContainer = CircleAvatar(
      radius: 50,
      backgroundColor: Color(0xff008080),
      child: CircleAvatar(
        radius: 45,
        backgroundImage: NetworkImage(avatarUrl),
      ),
    );

    final detailUser = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          userName,
          style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
              color: Color(0xff1d4963)),
        ),
        Text(
          userDesc,
          style: TextStyle(fontSize: 14.0, color: Color(0xff1d4963)),
        ),
        Padding(
          padding: EdgeInsets.only(top: 5.0),
          child: Text(userEmail,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
        )
      ],
    );

    return Container(
      margin: EdgeInsets.fromLTRB(10, heightTop, 10, 20),
      child: Wrap(
        spacing: 20.0,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: <Widget>[avatarContainer, detailUser],
      ),
    );
  }
}
