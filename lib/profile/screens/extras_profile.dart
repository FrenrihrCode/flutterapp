import 'package:flutter/material.dart';

class ExtrasProfile extends StatelessWidget {
  final int followers, photos, following;
  ExtrasProfile(this.followers, this.following, this.photos);
  @override
  Widget build(BuildContext context) {
    final followersContainerBox = Container(
        width: 100,
        padding: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            color: Color(0xFFe1edf8),
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Column(
          children: <Widget>[
            Text(followers.toString(),
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold)),
            Text('Seguidores', style: TextStyle(fontSize: 16.0))
          ],
        ));
    final followingContainerBox = Container(
        width: 100,
        padding: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            color: Color(0xFFe1edf8),
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Column(
          children: <Widget>[
            Text(following.toString(),
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold)),
            Text('Siguiendo', style: TextStyle(fontSize: 16.0))
          ],
        ));
    final photosContainerBox = Container(
        width: 100,
        padding: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            color: Color(0xFFe1edf8),
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Column(
          children: <Widget>[
            Text(photos.toString(),
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold)),
            Text('Fotos', style: TextStyle(fontSize: 16.0))
          ],
        ));

    return Center(
      child: Wrap(
        spacing: 5.0,
        children: [
          followersContainerBox,
          followingContainerBox,
          photosContainerBox
        ],
      ),
    );
  }
}
